#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
from dataclasses import dataclass
from typing import Any

from dotenv import load_dotenv

basedir: str = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


@dataclass
class BaseConfig:
    # Security
    SECRET_KEY: str = os.environ.get('SECRET_KEY') or '\x92\x103\x86\x0f\x9f\xb8\xe7JH_:,' \
                                                      '\x1f\xec?m\x9a\x8f\xbf\xfe\x1e9\xe1\xc1\xb2z\xa8\x9d*H\x8c'
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') or 'super_secret_key'
    # This can be an empty string, in which case the header contains only the
    # JWT (insead of something like HeaderName: Bearer <JWT>)
    JWT_HEADER_TYPE = os.environ.get('JWT_HEADER_TYPE') or ''

    # Database
    MONGO_URI: str = os.environ.get('MONGO_URI') or 'mongodb://mongo:27017/xls_parser'

    # Logging
    LOG_DIR: str = os.environ.get('LOG_DIR') or 'logs'
    LOG_LEVEL: Any = os.environ.get('LOG_LEVEL') or logging.INFO
    LOG_TO_STDOUT: bool = os.environ.get('LOG_TO_STDOUT')

    # Celery
    CELERY_BROKER_URL: str = os.environ.get('CELERY_BROKER_URL') or 'redis://redis:6379/0'
    CELERY_RESULT_BACKEND: str = os.environ.get('CELERY_RESULT_BACKEND') or 'redis://redis:6379/0'

    # SWAGGER
    SWAGGER_UI_JSONEDITOR = os.environ.get('SWAGGER_UI_JSONEDITOR') or True


@dataclass
class TestingConfig(BaseConfig):
    TESTING: bool = True


@dataclass
class DevelopmentConfig(BaseConfig):
    pass
