#!/usr/bin/env python
# -*- coding: utf-8 -*-

from celery import Celery
from flask import Flask
from flask_jwt_extended import JWTManager
from flask_restplus import Api
from flask_cors import CORS
from mongoengine import connect
from logging.handlers import RotatingFileHandler
import logging
import os

from config import DevelopmentConfig, BaseConfig
from .celery_utils import init_celery

authorization: dict = {
    'Bearer': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

jwt = JWTManager()

cors = CORS()

api: Api = Api(doc='/documentation',  # doc=False to disable Swagger documentation
               version='1.0.0',
               title='XLS-PARSING-SERVICE',
               description='Test API for Ingmar`s xls_files parser',
               authorizations=authorization)


def make_celery(app_name=__name__, config_class: BaseConfig = DevelopmentConfig) -> Celery:
    return Celery(app_name, broker=config_class.CELERY_BROKER_URL, backend=config_class.CELERY_RESULT_BACKEND)


celery: Celery = make_celery()


def create_app(config_class=DevelopmentConfig, **kwargs) -> Flask:
    """
    Factory for creating application instances
    :param config_class: Configuration class
    :return: Configured instance of Flask() application
    """
    app: Flask = Flask(__name__)
    app.config.from_object(config_class)
    if kwargs.get('celery'):
        init_celery(kwargs.get('celery'), app)

    # Connecting to the MongoDB by mongoengine module
    connect('XLS_Parser', host=config_class.MONGO_URI)

    from application.v1 import bp as api_bp

    api.init_app(api_bp)
    jwt.init_app(app)
    cors.init_app(app)

    app.register_blueprint(api_bp, url_prefix='/api/v1')
    api.add_namespace(ns_xls)
    api.add_namespace(ns_users)

    # Logging
    if not app.debug:
        if not os.path.exists(config_class.LOG_DIR):
            os.mkdir(config_class.LOG_DIR)
        file_handler = RotatingFileHandler('{}/xls_parser.log'.format(config_class.LOG_DIR),
                                           maxBytes=10485760,
                                           backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
        ))
        file_handler.setLevel(config_class.LOG_LEVEL)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(config_class.LOG_LEVEL)
        app.logger.info('XLS parser service startup')

    return app

from .v1.xls_files.parameters import ns as ns_xls
from .v1.users.parameters import ns as ns_users
from .helpers import jwt_tokens, errors
