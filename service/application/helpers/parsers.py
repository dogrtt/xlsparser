#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

from flask_restplus.reqparse import RequestParser
from werkzeug.datastructures import FileStorage

from application.v1.xls_files.parameters import ns

# Parser for XLS file uploading
file_upload: RequestParser = RequestParser()
file_upload.add_argument('xls_file',
                         type=FileStorage,
                         location='files',
                         required=True,
                         help='XLS file')


# TODO: Forgot to annotate kwargs
def parse_get_kwargs(**kwargs) -> Dict[str, any]:
    """
    Extract arguments from request
    :param kwargs: Arguments
    :return: dict: Kwargs as keys and values
    """
    parser: RequestParser = ns.parser()
    for kwarg in kwargs:
        parser.add_argument(str(kwarg), location='args', type=kwargs[kwarg])
    # strict=True - throw an error if request includes not specified params
    return parser.parse_args(strict=True)
