#!/usr/bin/env python
# -*- coding: utf-8 -*-

from application import jwt
from application.v1.users.models import User


@jwt.user_loader_callback_loader
def get_current_user(identity):
    user = User.objects(username=identity).first()
    return user if user else None


@jwt.user_loader_error_loader
def def_current_user_error(identity):
    return {'message': 'Cant load user {}'.format(identity)}, 401
