#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Generator, Any

from flask_restplus import abort, fields

from .custom_fields import CustomField


def get_list_chunks(lst, n) -> Generator[Any, None, None]:
    """
    List chunks generator
    :param: lst: list:
    :param: n: int: Chunk length
    """
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def validate_payload(payload, api_model):
    """
    Validate payload against an api_model. Aborts in case of failure
    - This function is for custom fields as they can't be validated by
      Flask-RESTplus automatically.
    - This is to be called at the start of a post or put method
    :param payload: api.payload
    :param api_model: api.model
    """
    # check if any required fields are missing in payload
    if type(payload) is not dict:
        abort(400)
    for key in api_model:
        if api_model[key].required and key not in payload:
            abort(400, 'Required field {} missing'.format(key))
    # check if any unexpected fields exists
    for key in payload:
        if key not in api_model:
            abort(422, 'Unexpected fields')
    # check payload
    for key in payload:
        field = api_model[key]
        if isinstance(field, fields.List):
            field = field.container
            data = payload[key]
        else:
            data = [payload[key]]
        if isinstance(field, CustomField) and hasattr(field, 'validate'):
            for i in data:
                if not field.validate(i):
                    abort(400, 'Validation of {} field failed'.format(key))
