#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_jwt_extended.exceptions import (NoAuthorizationError, WrongTokenError, FreshTokenRequired,
                                           UserLoadError)
from jwt.exceptions import ExpiredSignatureError

from application import api


@api.errorhandler(NoAuthorizationError)
def handle_no_auth_exception(error):
    return {'message': 'Authorization: Bearer <token> header is missing'}, 400


@api.errorhandler(ExpiredSignatureError)
def handle_expired_token_exception(error):
    return {'message': 'Granted token expired'}, 400


@api.errorhandler(WrongTokenError)
def handle_expired_token_exception(error):
    return {'message': 'Wrong token'}, 400


@api.errorhandler(FreshTokenRequired)
def handle_expired_token_exception(error):
    return {'message': 'Fresh token required'}, 400


@api.errorhandler(UserLoadError)
def handle_expired_token_exception(error):
    return {'message': 'User does not exist'}, 404
