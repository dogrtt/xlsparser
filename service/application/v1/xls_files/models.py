#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from typing import Dict, List, Any, Optional

import mongoengine as me


class GeologicalClass(me.EmbeddedDocument):
    name: me.StringField = me.StringField(required=True)
    code: me.StringField = me.StringField(required=True)

    def __str__(self) -> str:
        return '<GeologicalClass (name={self.name})>'.format(self=self)


class Section(me.EmbeddedDocument):
    name: me.StringField = me.StringField(required=True)
    geological_classes: List[GeologicalClass] = me.EmbeddedDocumentListField(GeologicalClass)

    def __str__(self) -> str:
        return '<Section (name={self.name})>'.format(self=self)


class XlsFile(me.Document):
    author: Optional[str] = me.StringField(required=True, max_length=256, default='undefined')
    name: str = me.StringField(required=True, max_length=256)
    file: Any = me.FileField()
    created: Any = me.DateTimeField(default=datetime.utcnow)
    sections: List[Section] = me.EmbeddedDocumentListField(Section)

    meta: Dict[str, str] = {'collection': 'uploaded_files'}

    def __str__(self) -> str:
        return '<XlsFile (name={self.name})>'.format(self=self)
