#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict, Any, Optional, List, Tuple, Union

from flask_jwt_extended import current_user, fresh_jwt_required
from flask_restplus import Resource, abort, reqparse
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from application.helpers import parsers
from application.v1.tasks import xls_parsing
from .models import XlsFile
from .parameters import ns


@ns.route('/', endpoint='xls_files_ep')
class XlsFilesResource(Resource):
    """
    XLS files endpoint for interaction with XLS file model queries
    """

    @ns.param('page', description='Page number :int:')
    @ns.param('per_page', description='Items per page :int:')
    def get(self) -> Dict[Any, Any]:
        """
        Get list of all uploaded XLS files
        It will be hurt If `sections` will contain a huge count of nested elements.
        Need something like lazy load or "pagination" for that. On Frontend caching required.
        :return: dict:
        """

        # Extract and check pagination params
        kwargs: Dict[str, int] = parsers.parse_get_kwargs(page=int, per_page=int)
        page: Optional[int] = kwargs['page']
        per_page: Optional[int] = kwargs['per_page']

        # TODO: Extract defaults to Config
        if not page:
            page = 1  # Default value
        if not per_page:
            per_page = 100  # Default value

        offset: int = (page - 1) * per_page

        files: List[Dict[Any, Any]] = XlsFile.objects.skip(offset).limit(per_page)

        return document_schema.dump(files)

    @ns.expect(parsers.file_upload)
    @fresh_jwt_required
    @ns.doc(security='Bearer')
    def post(self) -> Tuple[Dict[str, Union[str, Any]], int]:
        """
        Upload .XLS file to server
        :return: Status code
        """

        if not current_user:
            abort(400, 'Author not found')

        args: reqparse.ParseResult = parsers.file_upload.parse_args()
        storage: FileStorage = args['xls_file']

        # Create and save to DB new xls file document
        new_file: XlsFile = XlsFile()
        new_file.author = current_user.username
        new_file.name = secure_filename(storage.filename)
        new_file.file.put(storage, content_type='application/xls')
        new_file.save()

        # Start background task with Celery
        task = xls_parsing.parse_xls_file.apply_async(args=[str(new_file.id)])

        return {'message': 'File uploaded. Parsing started.', 'task_id': task.id}, 202


@ns.route('/status/<string:task_id>', endpoint='parsing_status_ep')
class ParsingStatusResource(Resource):
    """
    XLS files parsing process status endpoint
    """

    def get(self, task_id):
        """
        Get task status info by id
        :param task_id: str: Task ID
        :return: dict: Status data
        """
        task = xls_parsing.parse_xls_file.AsyncResult(task_id)
        if task.state == 'PENDING':
            # Job did not start yet
            response = {
                'state': task.state,
                'current': 0,
                'total': 100,
                'status': 'Pending...'
            }
        elif task.state != 'FAILURE':
            response = {
                'state': task.state,
                'current': task.info.get('current', 0),
                'total': task.info.get('total', 100),
                'status': task.info.get('status', '')
            }
            if 'result' in task.info:
                response['result'] = task.info['result']
        else:
            # Something went wrong in the background job
            response = {
                'state': task.state,
                'current': 1,
                'total': 1,
                'status': str(task.info),  # This is the exception raised
            }

        return response


from .schemas import XlsFileSchema

document_schema = XlsFileSchema(many=True)
