#!/usr/bin/env python
# -*- coding: utf-8 -*-

from marshmallow import Schema, fields

from .models import XlsFile, Section, GeologicalClass
from typing import Tuple, List


class GeologicalClassSchema(Schema):
    """
    Schema for `geological_classes` nested list in `Section` class
    """

    class Meta:
        model: object = GeologicalClass
        fields: Tuple[str] = ('name', 'code')


class SectionSchema(Schema):
    """
    Schema for `sections` nested list in `XlsFile` class
    """

    class Meta:
        model: object = Section
        fields: Tuple[str] = ('name', 'geological_classes')

    geological_classes: List[GeologicalClassSchema] = fields.Nested(GeologicalClassSchema, many=True)


class XlsFileSchema(Schema):
    """
    Schema for XLS file representation
    """

    class Meta:
        model: object = XlsFile
        fields: Tuple[str] = ('author', 'name', 'created', 'sections')

    sections: List[SectionSchema] = fields.Nested(SectionSchema, many=True)
