#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restplus import Namespace

ns: Namespace = Namespace('xls_files', description='XLS files related operations')
