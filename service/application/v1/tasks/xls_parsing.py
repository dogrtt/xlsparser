#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
from collections import OrderedDict
from typing import Dict

from pyexcel_xls import get_data as xls_get

from application import celery
from application.helpers import utils
from application.v1.xls_files.models import XlsFile, Section, GeologicalClass
from application.v1.xls_files.schemas import XlsFileSchema

document_schema: XlsFileSchema = XlsFileSchema()


@celery.task(bind=True)
def parse_xls_file(self, file_id: str) -> Dict[str, str]:
    """
    Async Celery task that load serialized file from DB by id and then parsing it to XlsFile's nested Sections
    :param self: Used to record the status updates
    :param file_id: str:
    :return: dict: Status info with updated file
    """
    file: XlsFile = XlsFile.objects(id=file_id).first()
    self.update_state(state='STARTED',
                      meta={'current': 0,
                            'total': 100,
                            'status': 'parsing'})
    file_data: OrderedDict = xls_get(io.BytesIO(file.file.read()))
    self.update_state(state='PROGRESS',
                      meta={'current': 0,
                            'total': 100,
                            'status': 'stored file parsing'})
    for sheet_name, sheet_data in file_data.items():
        data_len = float(len(sheet_data))
        for idx, row in enumerate(sheet_data):
            # Just for example.
            self.update_state(state='PROGRESS',
                              meta={'current': 50 + int(float(idx) / (data_len / 100.0)),
                                    'total': 100,
                                    'status': 'db filling'})
            # Skip header row.
            # Yeah, that's not unified for different xls internal formats.
            # But Ingmar did not ask for it... lol
            if not idx:
                continue

            section: Section = Section(name=row[0])
            for name, code in utils.get_list_chunks(row[1::], 2):
                section.geological_classes.append(GeologicalClass(name=name, code=code))
            file.sections.append(section)
    file.save()
    self.update_state(state='SUCCESS',
                      meta={'current': 100,
                            'total': 100,
                            'status': 'complete'})

    return {'status': 'SUCCESS',
            'current': 100,
            'total': 100,
            'result': document_schema.dump(file)}
