#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint

bp: Blueprint = Blueprint('v1', __name__)
