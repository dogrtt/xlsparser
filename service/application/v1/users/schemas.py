#!/usr/bin/env python
# -*- coding: utf-8 -*-

from marshmallow import Schema

from .models import User


class UserSchema(Schema):
    """
    Schema for `User` class representation
    """

    class Meta:
        model: User = User
        fields = ('username',)
