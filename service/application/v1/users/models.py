#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from typing import Dict, Any

import mongoengine as me
from werkzeug.security import generate_password_hash, check_password_hash


class User(me.Document):
    username: str = me.StringField(required=True, unique=True, max_length=32)
    password_hash: str = me.StringField(required=True, max_length=128)
    created: Any = me.DateTimeField(default=datetime.utcnow)

    meta: Dict[str, str] = {'collection': 'users'}

    def __str__(self):
        return '<User (username={self.username})>'.format(self=self)

    def set_password(self, password) -> None:
        self.password_hash = generate_password_hash(password)

    def check_password(self, password) -> bool:
        return check_password_hash(self.password_hash, str(password))
