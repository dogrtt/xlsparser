#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict, Any, Optional, Tuple

from flask_jwt_extended import fresh_jwt_required, current_user, create_access_token, create_refresh_token, \
    jwt_refresh_token_required, get_jwt_identity
from flask_restplus import Resource, abort

from application.helpers.utils import validate_payload
from flask import current_app as app
from .models import User
from .parameters import ns, users_name_pwd
from .schemas import UserSchema


@ns.route('/', endpoint='users_ep')
class UsersResource(Resource):
    """
    Users endpoint model for interaction with User model queries
    """

    @ns.expect(users_name_pwd)
    def post(self) -> Tuple[dict, int]:
        """
        Create new user by **username** and **password** if it is possible
        :return: dict: Report status of request
        """
        validate_payload(ns.payload, users_name_pwd)
        if User.objects(username=ns.payload['username']).first():
            app.logger.info('Someone try to signup with already existed username: <%s>', ns.payload['username'])
            abort(400, 'This username already engaged')
        user: User = User(username=ns.payload['username'])
        user.set_password(ns.payload['password'])
        user.save()
        app.logger.info('New User <%s> successfully signed up', ns.payload['username'])

        return users_schema.dump([user]), 201


@ns.route('/me', endpoint='me_ep')
@ns.header('Authorization: Bearer', 'JWT TOKEN', required=True)
class CurrentUserResource(Resource):
    """
    Current user endpoint
    """

    @fresh_jwt_required
    @ns.doc(security='Bearer')
    def get(self) -> Dict[str, str]:
        """
        Current user data
        :return: dict: Authorized user data
        """
        if not current_user:
            app.logger.info('Someone try to retrieve self info without userdata in JWT')
            abort(404, 'Current user not found')

        app.logger.info('User <%s> successfully retrieve self info', str(current_user))

        return users_schema.dump([current_user])


@ns.route('/login', endpoint='login_ep')
class LoginResource(Resource):
    """
    Login endpoint
    """

    @ns.expect(users_name_pwd)
    def post(self) -> Tuple[Dict[str, str], int]:
        """
        Check username and password, then login
        :return: dict: Access and Refresh tokens
        """
        validate_payload(ns.payload, users_name_pwd)
        user: Optional[User] = User.objects(username=ns.payload['username']).first()
        if not user:
            app.logger.info('Someone try to login with incorrect username: <%s>', ns.payload['username'])
            abort(404)
        if not user.check_password(ns.payload['password']):
            app.logger.info('User <%s> try to login with wrong password', ns.payload['username'])
            abort(401)
        response: Dict[str, str] = {
            'access_token': create_access_token(identity=ns.payload['username'], fresh=True),
            'refresh_token': create_refresh_token(identity=ns.payload['username'])
        }

        app.logger.info('User <%s> successfully logged in', ns.payload['username'])

        return response, 200


@ns.route('/fresh-login', endpoint='fresh_login_ep')
class FreshLoginResource(Resource):
    """
    Fresh login endpoint
    """

    @ns.expect(users_name_pwd)
    def post(self) -> Tuple[Dict[str, str], int]:
        """
        Use if you need a fresh token for a user (with verified username and password)
        :return: dict: New fresh token
        """
        validate_payload(ns.payload, users_name_pwd)
        user: Optional[User] = User.objects(username=ns.payload['username']).first()
        if not user:
            app.logger.info('Someone try to fresh login with incorrect username: <%s>', ns.payload['username'])
            abort(404)
        if not user.check_password(ns.payload['password']):
            app.logger.info('User <%s> try to fresh login with wrong password', ns.payload['username'])
            abort(401)
        response: Dict[str, str] = {
            'access_token': create_access_token(identity=ns.payload['username'], fresh=True)
        }

        app.logger.info('User <%s> successfully fresh logged in', ns.payload['username'])

        return response, 200


@ns.route('/refresh_token', endpoint='refresh_token_ep')
class RefreshTokenResource(Resource):
    """
    Resource that receive jwt_refresh_token and return new access_token
    """

    @jwt_refresh_token_required
    @ns.doc(security='Bearer')
    def post(self) -> Tuple[Dict[str, str], int]:
        """
        Search refresh_token in headers and generate access token
        :return: dict: Access token
        """
        identity: Optional[Any] = get_jwt_identity()
        if not identity:
            abort(401, 'Token is not valid')
        response: Dict[str, str] = {
            'access_token': create_access_token(identity=identity, fresh=False)
        }
        return response, 200


users_schema: UserSchema = UserSchema(many=True)
