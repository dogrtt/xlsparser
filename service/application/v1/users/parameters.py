#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_restplus import Namespace

import application.helpers.custom_fields as custom_fields

ns: Namespace = Namespace('users', description='Users related operations')

# Model that take username and password for creating new user
users_name_pwd = ns.model('UserNew', {
    'username': custom_fields.String('Put username here', required=True),
    'password': custom_fields.String('Put password here', required=True)
})
