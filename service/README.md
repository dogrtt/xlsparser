##General
####Swagger documentation for API
http://127.0.0.1:5000/api/v1/documentation

####Logging
Logs with **INFO** level disabled in **FLASK_DEBUG** mode 


##Celery
####For workers start:
````
celery worker -A celery_worker.celery --loglevel=info --pool=solo
````
