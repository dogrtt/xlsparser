#!/usr/bin/env python
# -*- coding: utf-8 -*-

from application import create_app
from flask import Flask

app: Flask = create_app()
