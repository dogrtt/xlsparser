#!/usr/bin/env python
# -*- coding: utf-8 -*-

from application import celery, create_app
from application.celery_utils import init_celery
app = create_app()
init_celery(celery, app)
