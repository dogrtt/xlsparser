interface User {
    accessToken?: string,
    refreshToken?: string,
}

export const getToken = (isRefresh: boolean = false): string => {
    let storagedUser: string | null = localStorage.getItem('current_user');
    let token: string = '';
    if (!Boolean(storagedUser)) {
        return token;
    }

    let user: User = JSON.parse(storagedUser!);

    if (user) {
        if (user.refreshToken != undefined && user.accessToken != undefined) {
            token = 'Bearer ' + isRefresh ? user.refreshToken : user.accessToken;
        }
    }
    return token;
};
