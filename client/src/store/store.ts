import {applyMiddleware, createStore, StoreCreator} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import thunkMiddleware from 'redux-thunk';
import rootReducer from './_reducers'
import {I_Store} from "../models/common.models";


// TODO: Replace `any`
const configureStore: StoreCreator = (initialState: any) => {
    return createStore (
        rootReducer,
        initialState,
        composeWithDevTools(
            applyMiddleware(
                thunkMiddleware
            )
        )
    )
};

const defaultState: I_Store = {
    users: {
        items: [],
        currentUser: {
            username: '',
            accessToken: '',
            refreshToken: '',
            signUp: {
                isPending: false,
                error: null,
            },
            signIn: {
                isPending: false,
                error: null,
            },
        },
    },
    xls: {
        isPending: false,
        error: null,
        adding: {
            isPending: false,
            error: null,
            tasks: [],
        },
        documents: [
            {
                id: '',
                name: '',
                author: '',
                created: '',
                sections: [
                    {
                        name: '',
                        geological_classes: [
                            {
                                name: '',
                                code: '',
                            },
                        ],
                    },
                ],
                updating: {
                    isPending: false,
                    error: null,
                },
                deleting: {
                    isPending: false,
                    error: null,
                },
                parsing: {
                    isPending: false,
                    error: null,
                },
            },
        ],
    },
};

// TODO: Set properly type of I_Store
// @ts-ignore
const store = configureStore(defaultState);

export default store;
