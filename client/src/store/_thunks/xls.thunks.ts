import ACTIONS from '../_actions';
import {I_XlsDocument} from "../../models/xls.models";


const fetchXlsDocuments = () => (dispatch: any) => {
    const headers = new Headers();

    headers.append("Accept", "application/json");

    const options = {
        method: 'GET',
        headers
    };

    const route = 'http://localhost:4242/api/v1/xls_files/';
    dispatch(ACTIONS.xlsActions.fetchXlsPending());
    return fetch(route, options)
        .then(res => {
            return res.json();
        })
        .then(res => {
            if (res.errors) {
                throw(res.title);
            }
            dispatch(ACTIONS.xlsActions.fetchXlsStatusSuccess(res));
            return res;
        })
        .catch(error => {
            dispatch(ACTIONS.xlsActions.fetchXlsError(error));
        })
};

const addXlsFile = (xls_file: any) => (dispatch: any) => {
    const headers = new Headers();
    const token = JSON.parse(<string>localStorage.getItem('current_user')).access_token;

    headers.append("Accept", 'application/json');
    headers.append('Authorization', `${token}`);
    headers.delete('Content-Type');

    const options = {
        method: 'POST',
        body: xls_file,
        headers
    };

    const route = 'http://localhost:4242/api/v1/xls_files/';
    dispatch(ACTIONS.xlsActions.addXlsPending());
    return fetch(route, options)
        .then(res => {
            return res.json()
        })
        .then(res=>{
            if (res.msg || res.errors) {
                throw(res);
            }
            dispatch(ACTIONS.xlsActions.addXlsSuccess(res));
            return res;
        })
        .catch(error => {
            dispatch(ACTIONS.xlsActions.addXlsError(error));
        })
};

export default {
    fetchXlsDocuments,
        addXlsFile
}
