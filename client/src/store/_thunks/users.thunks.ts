import ACTIONS from '../_actions';
import {I_UserCredential} from "../../models/user.models";

const addUser = (data: I_UserCredential) => (dispatch: any) => {
    const headers = new Headers();

    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");

    const options = {
        method: 'POST',
        body: JSON.stringify(data),
        headers
    };
    const route = 'http://localhost:4242/api/v1/users/';
    dispatch(ACTIONS.usersActions.addUserPending());
    return fetch(route, options)
        .then(res => {
            return res.json();
        })
        .then(res => {
            if (res.errors) {
                throw(res.title);
            }
            dispatch(ACTIONS.usersActions.addUserSuccess(res));
            return res;
        })
        .catch(error => {
            dispatch(ACTIONS.usersActions.addUserError(error))
        })
};

const loginUser = (data: I_UserCredential, history: any) => (dispatch: any) => {
    const headers = new Headers();

    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");

    const options = {
        method: 'POST',
        body: JSON.stringify(data),
        headers
    };
    const route = 'http://localhost:4242/api/v1/users/login';
    dispatch(ACTIONS.usersActions.loginUserPending());
    return fetch(route, options)
        .then(res => {
            return res.json();
        })
        .then(res => {
            if (res.errors) {
                throw (res.title);
            }
            dispatch(ACTIONS.usersActions.loginUserSuccess(res));
            localStorage.setItem('current_user', JSON.stringify(res));
            history.push('/');
            return res;
        })
        .catch(error => {
            dispatch(ACTIONS.usersActions.loginUserError(error))
        })
};

export default {
    addUser,
    loginUser,
}
