import ACTIONS from '../_actions'
import _ from 'lodash';
import {I_Action} from "../../models/common.models";
import {I_UsersStore} from "../../models/user.models";


const defaultState: I_UsersStore = {
        items: [],
        currentUser: {
            username: '',
            accessToken: '',
            refreshToken: '',
            signUp: {
                isPending: false,
                error: null,
            },
            signIn: {
                isPending: false,
                error: null,
            },
        },
};

const usersReducer = (state = defaultState, action: I_Action) => {
    switch (action.type) {
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // ADD
        case ACTIONS.Types.ADD_USER_PENDING: {
            let newState = _.cloneDeep(state);
            newState.currentUser.signUp.isPending = true;
            newState.currentUser.signUp.error = null;
            return newState;
        }

        case ACTIONS.Types.ADD_USER_SUCCESS: {
            let newState = _.cloneDeep(state);
            newState.currentUser.signUp.isPending = false;
            newState.currentUser.signUp.error = null;
            return newState;
        }

        case ACTIONS.Types.ADD_USER_ERROR: {
            let newState = _.cloneDeep(state);
            newState.currentUser.signUp.isPending = false;
            newState.currentUser.signUp.error = action.payload;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // LOGIN
        case ACTIONS.Types.LOGIN_USER_PENDING: {
            let newState = _.cloneDeep(state);
            newState.currentUser.signIn.isPending = true;
            newState.currentUser.signIn.error = null;
            return newState;
        }

        case ACTIONS.Types.LOGIN_USER_SUCCESS: {
            let newState = _.cloneDeep(state);
            newState.currentUser.accessToken = action.payload.accessToken;
            newState.currentUser.refreshToken = action.payload.refreshToken;
            newState.currentUser.username = action.payload.name;
            newState.currentUser.signIn.isPending = false;
            newState.currentUser.signIn.error = null;
            return newState;
        }

        case ACTIONS.Types.LOGIN_USER_ERROR: {
            let newState = _.cloneDeep(state);
            newState.currentUser.accessToken = '';
            newState.currentUser.refreshToken = '';
            newState.currentUser.username = '';
            newState.currentUser.signIn.isPending = false;
            newState.currentUser.signIn.error = action.payload.error;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // DEFAULT
        default:
            return state;
        }
};

export default usersReducer;
