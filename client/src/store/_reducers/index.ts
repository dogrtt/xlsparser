import {combineReducers} from "redux";
import usersReducer from "./users.reduser";
import xlsReducer from "./xls.reducer";


export default combineReducers({
    users: usersReducer,
    xls: xlsReducer
});
