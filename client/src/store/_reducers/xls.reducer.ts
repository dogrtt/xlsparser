import {I_Action} from "../../models/common.models";
import ACTIONS from "../_actions";
import _ from "lodash";
import {I_XlsStore} from "../../models/xls.models";


const defaultState: I_XlsStore = {
    isPending: false,
    error: null,
    adding: {
        isPending: false,
        error: null,
        tasks: [],
    },
    documents: [
        {
            id: '',
            name: '',
            author: '',
            created: '',
            sections: [
                {
                    name: '',
                    geological_classes: [
                        {
                            name: '',
                            code: '',
                        },
                    ],
                },
            ],
            updating: {
                isPending: false,
                error: null,
            },
            deleting: {
                isPending: false,
                error: null,
            },
            parsing: {
                isPending: false,
                error: null,
            },
        },
    ],
};

const xlsReducer = (state = defaultState, action: I_Action) => {
    switch (action.type) {
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // FETCH
        case ACTIONS.Types.FETCH_XLS_PENDING: {
            let newState = _.cloneDeep(state);
            newState.isPending = true;
            newState.error = null;
            return newState;
        }
        case ACTIONS.Types.FETCH_XLS_SUCCESS: {
            let newState = _.cloneDeep(state);
            newState.isPending = false;
            newState.error = null;
            newState.documents = action.payload;
            return newState;
        }
        case ACTIONS.Types.FETCH_XLS_ERROR: {
            let newState = _.cloneDeep(state);
            newState.isPending = false;
            newState.error = action.payload;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // ADD
        case ACTIONS.Types.ADD_XLS_PENDING: {
            let newState = _.cloneDeep(state);
            newState.adding.isPending = true;
            newState.adding.error = null;
            return newState;
        }

        case ACTIONS.Types.ADD_XLS_SUCCESS: {
            let newState = _.cloneDeep(state);
            newState.adding.tasks.push(action.payload.task_id);
            newState.adding.isPending = false;
            newState.adding.error = null;
            return newState;
        }

        case ACTIONS.Types.ADD_USER_ERROR: {
            let newState = _.cloneDeep(state);
            newState.adding.isPending = false;
            newState.adding.error = action.payload;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // DELETE
        case ACTIONS.Types.DELETE_XLS_PENDING: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].deleting.isPending = true;
            newState.documents[index].deleting.error = null;
            return newState;
        }

        case ACTIONS.Types.DELETE_XLS_SUCCESS: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].deleting.isPending = false;
            newState.documents[index].deleting.error = null;
            _.remove(newState.documents, {id: action.payload});
            return newState;
        }

        case ACTIONS.Types.DELETE_XLS_ERROR: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].deleting.isPending = false;
            newState.documents[index].deleting.error = action.payload.error;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // UPDATE
        case ACTIONS.Types.UPDATE_XLS_PENDING: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].updating.isPending = true;
            newState.documents[index].updating.error = null;
            return newState;
        }

        case ACTIONS.Types.UPDATE_XLS_SUCCESS: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents.splice(index, 1, action.payload);
            newState.documents[index].updating.isPending = false;
            newState.documents[index].updating.error = null;
            return newState;
        }

        case ACTIONS.Types.UPDATE_XLS_ERROR: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].updating.isPending = false;
            newState.documents[index].updating.error = action.payload.error;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // FETCH STATUS
        case ACTIONS.Types.FETCH_XLS_STATUS_PENDING: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].parsing.isPending = true;
            newState.documents[index].parsing.error = null;
            return newState;
        }
        case ACTIONS.Types.FETCH_XLS_STATUS_SUCCESS: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents.splice(index, 1, action.payload);
            newState.documents[index].parsing.isPending = false;
            newState.documents[index].parsing.error = null;
            return newState;
        }
        case ACTIONS.Types.FETCH_XLS_STATUS_ERROR: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.documents, {id: action.payload.id});
            newState.documents[index].parsing.isPending = false;
            newState.documents[index].parsing.error = action.payload;
            return newState;
        }
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // DEFAULT
        default:
            return state;
    }
};

export default xlsReducer;
