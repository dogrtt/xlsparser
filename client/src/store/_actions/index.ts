import usersActions from "./users.actions";
import xlsActions from "./xls.actions";
import Types from "./action.types";

export default {
    usersActions,
    xlsActions,
    Types
}
