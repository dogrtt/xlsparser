import Types from "./action.types";
import {I_CurrentUser} from "../../models/user.models";
import {I_Error} from "../../models/common.models";


// + Add
const addUserPending = () => ({
    type: Types.ADD_USER_PENDING,
});

const addUserSuccess = (username: string) => ({
    type: Types.ADD_USER_SUCCESS,
    payload: username
});

const addUserError = (error: I_Error) => ({
    type: Types.ADD_USER_SUCCESS,
    payload: error
});
// - Add

// + Login
const loginUserPending = () => ({
    type: Types.LOGIN_USER_PENDING,
});

const loginUserSuccess = (user: I_CurrentUser) => ({
    type: Types.LOGIN_USER_SUCCESS,
    payload: user
});

const loginUserError = (error: I_Error) => ({
    type: Types.LOGIN_USER_SUCCESS,
    payload: error
});
// - Login

export default {
    addUserPending,
    addUserSuccess,
    addUserError,

    loginUserPending,
    loginUserSuccess,
    loginUserError
}
