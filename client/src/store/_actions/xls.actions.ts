import Types from "./action.types";
import {I_XlsDocument, I_XlsParsingStatus, I_XlsUploadedReport} from "../../models/xls.models";
import {I_Error} from "../../models/common.models";


// + Add
const addXlsPending = () => ({
    type: Types.ADD_XLS_PENDING,
});

const addXlsSuccess = (report: I_XlsUploadedReport) => ({
    type: Types.ADD_XLS_SUCCESS,
    payload: report,
});

const addXlsError = (error: I_Error) => ({
    type: Types.ADD_XLS_ERROR,
    payload: error,
});
// - Add

// + Fetch
const fetchXlsPending = () => ({
    type: Types.FETCH_XLS_PENDING,
});

const fetchXlsSuccess = (documents: I_XlsDocument[]) => ({
    type: Types.FETCH_XLS_SUCCESS,
    payload: documents,
});

const fetchXlsError = (error: I_Error) => ({
    type: Types.FETCH_XLS_ERROR,
    payload: error,
});
// - Fetch

// + Update
const updateXlsPending = () => ({
    type: Types.UPDATE_XLS_PENDING,
});

const updateXlsSuccess = (document: I_XlsDocument) => ({
    type: Types.UPDATE_XLS_SUCCESS,
    payload: document,
});

const updateXlsError = (error: I_Error) => ({
    type: Types.UPDATE_XLS_ERROR,
    payload: error,
});
// - Update

// + Delete
const deleteXlsPending = () => ({
    type: Types.DELETE_XLS_PENDING,
});

const deleteXlsSuccess = (document: I_XlsDocument) => ({
    type: Types.DELETE_XLS_SUCCESS,
    payload: document,
});

const deleteXlsError = (error: I_Error) => ({
    type: Types.DELETE_XLS_ERROR,
    payload: error,
});
// - Delete

// + Fetch Status
const fetchXlsStatusPending = () => ({
    type: Types.FETCH_XLS_PENDING,
});

const fetchXlsStatusSuccess = (status: I_XlsParsingStatus) => ({
    type: Types.FETCH_XLS_SUCCESS,
    payload: status,
});

const fetchXlsStatusError = (error: I_Error) => ({
    type: Types.FETCH_XLS_ERROR,
    payload: error,
});
// - Fetch Status

export default {
    addXlsPending,
    addXlsSuccess,
    addXlsError,
    
    fetchXlsPending,
    fetchXlsSuccess,
    fetchXlsError,

    updateXlsPending,
    updateXlsSuccess,
    updateXlsError,
    
    deleteXlsPending,
    deleteXlsSuccess,
    deleteXlsError,

    fetchXlsStatusPending,
    fetchXlsStatusSuccess,
    fetchXlsStatusError,
}
