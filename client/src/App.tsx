import React from 'react';
import './App.css';
import {Route} from 'react-router';
import {Switch} from "react-router-dom";
import HomePage from "./components/Home4";
import {PrivateRoute} from "./components/_auth/PrivateRoute";
import SignIn from "./components/_auth/SignIn";

const App: React.FC = () => {
  return (
    <Switch>
      <PrivateRoute exact path='/' component={HomePage} />
      <Route path={'/login'} component={SignIn} />
    </Switch>
  );
};

export default App;
