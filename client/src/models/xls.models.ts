export interface I_GeoClass {
    name: string,
    code: string
}

export interface I_Section {
    name: string,
    geological_classes: I_GeoClass[]
}

export interface I_XlsDocument {
    id: string,
    author: string,
    name: string,
    created: string,
    sections: I_Section[]
    updating: I_Pendable,
    deleting: I_Pendable,
    parsing: I_Pendable,
}

export interface I_XlsParsingStatus {
    current: number,
    total: number,
    status: string
}

export interface I_XlsUploadedReport {
    message: string,
    task_id: string
}

export interface I_Pendable {
    isPending: boolean,
    error: string | null
}

export interface I_XlsStore {
    isPending: boolean,
    error: string | null,
    adding: {
        isPending: boolean,
        error: string | null
        tasks: string[]
    },
    documents: I_XlsDocument[],
}
