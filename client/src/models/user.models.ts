import {I_Pendable} from "./xls.models";

export interface I_CurrentUser {
    username: string,
    accessToken: string,
    refreshToken: string,
    signUp: I_Pendable,
    signIn: I_Pendable,
}

export interface I_UserCredential {
    username: string,
    password: string
}

export interface I_UsersStore {
    items: string[],
    currentUser: I_CurrentUser
}
