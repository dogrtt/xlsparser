import {I_UsersStore} from "./user.models";
import {I_XlsStore} from "./xls.models";
import {Reducer} from "react";

export interface I_Error {
    message: string,
    code: number
}

export interface I_Action {
    type: string,
    payload: any
}

export interface I_Store {
    users: I_UsersStore,
    xls: I_XlsStore
}
