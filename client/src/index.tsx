import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from "./store/store";

const baseUrl: string | any = (document.getElementsByTagName('base')[0] || {}).href;
const rootElement: HTMLElement | null = document.getElementById('root');


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter basename={baseUrl}>
            <App/>
        </BrowserRouter>
    </Provider>,
    rootElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
