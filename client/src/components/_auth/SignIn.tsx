import React from 'react';

import {
    Avatar,
    Box,
    Button,
    Container,
    CssBaseline,
    Link,
    makeStyles,
    TextField,
    Tooltip,
    Typography,
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Thunks from '../../store/_thunks/users.thunks';
import {useDispatch} from "react-redux";
import {useHistory} from 'react-router-dom';


const Copyright = () => (
    <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://www.linkedin.com/in/dogrtt/">
            Philipp Bondarev.
        </Link>
        {' '}
        {new Date().getFullYear()}
        {'.'}
    </Typography>
);

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 0),
    },
}));

export default () => {
    const classes = useStyles();
    const [username, setUsername] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');

    const history = useHistory();
    const dispatch = useDispatch();

    const onLoginButtonClick = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();

        dispatch(Thunks.loginUser({
            username,
            password
        }, history))
    };

    const onRegisterButtonClick = (event: React.MouseEvent<HTMLElement>) => {
        event.preventDefault();

        dispatch(Thunks.addUser({
            username,
            password
        }))
    };

    const handleChange = (event: any) => {
        event.preventDefault();

        switch (event.target.id) {
            case 'username': {
                setUsername(event.target.value);
                break;
            }
            case 'password': {
                setPassword(event.target.value);
                break;
            }
        }
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    XLS Parser
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoFocus
                        value={username}
                        onChange={handleChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={handleChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={onLoginButtonClick}
                    >
                        Login
                    </Button>
                    <Tooltip title="Just enter username and password of new user, than click this button.">
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={onRegisterButtonClick}
                        >
                            Register
                        </Button>
                    </Tooltip>
                </form>
            </div>
            <Box mt={8}>
                <Copyright/>
            </Box>
        </Container>
    );
}
