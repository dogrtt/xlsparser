import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Button, Container, Paper, Table, TableBody, TableCell, TableHead, TableRow} from '@material-ui/core'
import Thunks from '../store/_thunks/xls.thunks';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        nested: {
            paddingLeft: theme.spacing(4),
        },
        paper: {
            padding: theme.spacing(2),
            display: 'flex',
            overflow: 'auto',
            flexDirection: 'column',
        },
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4),
        },
        input: {
            display: 'none',
        },
    }),
);

const GeoClassRow = (props: any) => {
    const {geoclass} = props;

    return (
        <React.Fragment>
            <TableRow>
                <TableCell colSpan={3}>{geoclass.name}</TableCell>
                <TableCell colSpan={3}>{geoclass.code}</TableCell>
            </TableRow>
        </React.Fragment>
    )
};

const SectionRow = (props: any) => {
    const {section} = props;

    return (
        <React.Fragment>
            <TableRow>
                <TableCell colSpan={6}>Section: {section.name}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell colSpan={3}>Geo Class Name</TableCell>
                <TableCell colSpan={3}>Geo Class Code</TableCell>
            </TableRow>
            {section.geological_classes.map((geoClass: any, index: number) => {
                return <GeoClassRow geoclass={geoClass} key={index}/>
            })}
        </React.Fragment>
    )
};

const DocumentRow = (props: any) => {
    const {doc} = props;

    return (
        <React.Fragment>
            <TableRow>
                <TableCell colSpan={2}>Document name: {doc.name}</TableCell>
                <TableCell colSpan={2}>Author: {doc.author}</TableCell>
                <TableCell colSpan={2}>Created: {doc.created}</TableCell>
            </TableRow>
            {doc.sections.map((section: any, index: number) => {
                return <SectionRow section={section} key={index}/>
            })}
        </React.Fragment>
    )
};

const HomePage = () => {
    const classes = useStyles();
    const xls = useSelector((state: any) => state.xls);
    const dispatch = useDispatch();

    const handleAddDocument = (event: any) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('xls_file', event.target.files[0]);
        dispatch(Thunks.addXlsFile(formData));
    };

    useEffect(()=>{
        dispatch(Thunks.fetchXlsDocuments())},
        [dispatch]
    );

    return (
        <Container maxWidth='lg' className={classes.container}>
            <Paper className={classes.root}>
                <Table aria-label='spanning table'>
                    <TableHead>
                        <TableCell colSpan={3}>XLS Documents</TableCell>
                        <TableCell align='right' colSpan={3}>
                            <input
                                accept="application/vnd.ms-excel"
                                className={classes.input}
                                id="contained-button-file"
                                multiple
                                type="file"
                                onChange={handleAddDocument}
                            />
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" color="primary" component="span">
                                    Upload
                                </Button>
                            </label>
                        </TableCell>
                    </TableHead>
                    <TableBody>
                        {xls.documents.map((doc: any, index: number) => {
                            return <DocumentRow doc={doc} key={index}/>
                        })}
                    </TableBody>
                </Table>
            </Paper>
        </Container>
    )
};

export default HomePage;