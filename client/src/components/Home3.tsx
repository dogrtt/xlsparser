import React, {useState} from 'react';
import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-balham.css';
import {AgGridReact} from "@ag-grid-community/react";
import {AllCommunityModules} from '@ag-grid-community/all-modules';


const HomePage: React.FC = () => {
    const cols = [{
        headerName: "Make", field: "make"
    }, {
        headerName: "Model", field: "model"
    }, {
        headerName: "Price", field: "price"
    }];

    const rows = [{
        make: "Toyota", model: "Celica", price: 35000
    }, {
        make: "Ford", model: "Mondeo", price: 32000
    }, {
        make: "Porsche", model: "Boxter", price: 72000
    }];

    return (
        <div
            className="ag-theme-balham"
            style={{
                height: '500px',
                width: '100%' }}
        >
            <AgGridReact
                columnDefs={cols}
                rowData={rows}
                modules={AllCommunityModules}>
        </AgGridReact>
        </div>
    )
};

export default HomePage;
