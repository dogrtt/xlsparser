import React, {forwardRef, useState} from 'react';
import {useSelector} from "react-redux";
import MaterialTable from "material-table";
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import {I_XlsDocument, I_Section, I_GeoClass} from "../models/xls.models";
import {Collapse, List, ListItem, ListItemText} from '@material-ui/core';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import { FixedSizeList } from 'react-window';


const tableIcons = {
    Add: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <AddBox {...props} ref={ref}/>),
    Check: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Check {...props} ref={ref}/>),
    Clear: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Clear {...props} ref={ref}/>),
    Delete: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <DeleteOutline {...props} ref={ref}/>),
    DetailPanel: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <ChevronRight {...props} ref={ref}/>),
    Edit: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Edit {...props} ref={ref}/>),
    Export: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <SaveAlt {...props} ref={ref}/>),
    Filter: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <FilterList {...props} ref={ref}/>),
    FirstPage: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <FirstPage {...props} ref={ref}/>),
    LastPage: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <LastPage {...props} ref={ref}/>),
    NextPage: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <ChevronRight {...props} ref={ref}/>),
    PreviousPage: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <ChevronLeft {...props} ref={ref}/>),
    ResetSearch: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Clear {...props} ref={ref}/>),
    Search: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Search {...props} ref={ref}/>),
    SortArrow: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <ArrowUpward {...props} ref={ref}/>),
    ThirdStateCheck: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <Remove {...props} ref={ref}/>),
    ViewColumn: forwardRef((props, ref: React.Ref<SVGSVGElement>) => <ViewColumn {...props} ref={ref}/>)
};

const Row: React.FC = (props: any) => {
    const {data, index} = props;
    const {
        sections,
        open,
        handleClick,
        selectedIndex}
    = data;

    const value: I_Section = sections[index];

    return (
        <React.Fragment key={index}>
            <ListItem
                button
                selected={selectedIndex === index}
                key={index}
                onClick={event => {
                    handleClick(event, index);
                }}>
                <ListItemText primary={value.name + ': Geo Classes'}/>
                {open && (selectedIndex === index) ? <ExpandLess/> : <ExpandMore/>}
            </ListItem>
            <Collapse
                in={open && (selectedIndex === index)}
                timeout="auto"
                unmountOnExit
                key={index}
            >
                <MaterialTable
                    title={'Geo Classes'}
                    icons={tableIcons}
                    columns={[
                        {title: 'Name', field: 'name'},
                        {title: 'Author', field: 'code'},
                    ]}
                    data={value.geological_classes}
                    components={{
                        Toolbar: props => (
                            <div></div>
                        )
                    }}
                />
            </Collapse>
        </React.Fragment>
    );
};

const VirtualizedSectionList = (props: any) => {
    const {data} = props;
    const [open, setOpen] = useState(false);
    const [selectedIndex, setSelectedIndex] = React.useState(-1);

    const handleClick = (event: React.MouseEvent<HTMLElement>, index: number) => {
        setOpen(!open);
        setSelectedIndex(index);

    };

    return (
        <FixedSizeList
            height={400}
            width='100%'
            itemSize={20}
            itemCount={data.length}
            itemData={{
                sections: data,
                selectedIndex,
                open,
                handleClick,
            }}
        >
            {Row}
        </FixedSizeList>
    )
};

const HomePage = () => {
    const xls = useSelector((state: any) => state.xls);
    const [selectedRow, setSelectedRow] = useState();

    return (
        <MaterialTable
            title='XLS Documents'
            icons={tableIcons}
            columns={[
                {title: 'Name', field: 'name'},
                {title: 'Author', field: 'author', editable: 'never'},
                {title: 'Created', field: 'created', editable: 'never'},
            ]}
            data={xls.documents}
            detailPanel={(rowData: I_XlsDocument) => {
                return (
                    <VirtualizedSectionList data={rowData.sections}/>
                )
            }
            }
            onRowClick={(event: any, rowData: any, togglePanel: any) => {
                togglePanel();
                setSelectedRow(rowData)
            }}
            options={{
                rowStyle: rowData => ({
                    backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id? '#EEE' : '#FFF')
                })
            }}
        />
    )
};

export default HomePage;
